# ----------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------Libraries-----------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------

import os
import json
import luigi

from datetime import datetime

# ----------------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------Classes-----------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------


class extract(luigi.Task):
    """
    Extract overages
    """

    # Refer to config JSON for specs and designations
    with open('config.json') as config:
        config = json.load(config)
    
    date = datetime.today().strftime('%m-%d-%Y')
    
    def requires(self):
        return None
    
    def output(self):
        os.chdir(self.config['reports'])
        return luigi.LocalTarget('Dekalb ' + self.date + '.xlsx')
    
    def run(self):
        os.chdir(self.config['pwd'])
        os.system('scrapy crawl dekalb')


class filter(luigi.Task):
    """
    Identify new excesses and mark their correlating indexes
    """

    # Refer to config JSON for specs and designations
    with open('config.json') as config:
        config = json.load(config)

    def requires(self):
        return dekalb()

    def output(self):
        os.chdir(self.config['reports'])
        return luigi.LocalTarget('Dekalb ' + self.date + '.xlsx')

    def run(self):
        os.chdir(self.config['rig'])
        os.system('python filter.py')
        

class rig(luigi.Task):
    """
    Differentiate between valid or invalid targets and establish set arguments
    """

    # Refer to config JSON for specs and designations
    with open('config.json') as config:
        config = json.load(config)

    def requires(self):
        return dekalb(), filter_dekalb()

    def output(self):
        os.chdir(self.config['reports'])
        return luigi.LocalTarget('Dekalb ' + self.date + '.xlsx')

    def run(self): 
        os.chdir(self.config['rig'])
        os.system('python rig.py')

        
class skip(luigi.Task):
    """
    Commence skip tracing operations
    """
    
    # Refer to config JSON for specs and designations
    with open('config.json') as config:
        config = json.load(config)
    
    def requires(self):
        return dekalb(), filter_dekalb(), rig()

    def output(self):
        os.chdir(self.config['processed'])
        return luigi.LocalTarget('Dekalb ' + self.date + '.xlsx')

    def run(self):
        os.chdir(self.config['skip'])
        os.system('python Skip_Tracer.py')


class store(luigi.Task):
    """
    Upload final transformation to data warehouse
    """

    # Refer to config JSON for specs and designations
    with open('config.json') as config:
        config = json.load(config)

    def requires(self):
        return dekalb(), filter_dekalb(), rig(), skip()

    def output(self):
        os.chdir(self.config['logs'])
        return luigi.LocalTarget('Dekalb ' + self.date + '.txt')

    def run(self):
        os.chdir(self.config['upload'])
        os.system('python upload.py')   


# ----------------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------Runtime-----------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------

if __name__ == '__main__':
    luigi.run()
